﻿using System.ComponentModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpf_parent_binding;

public class RelayCommand : ICommand
{
    Action m_action;

    public event EventHandler? CanExecuteChanged;

    public RelayCommand(Action action)
    {
        m_action = action;
    }

    public bool CanExecute(object? parameter)
    {
        return true;
    }

    public void Execute(object? parameter)
    {
        m_action.Invoke();
    }
}

public class SubItemViewModel
{
    public string SubItemProp1
    {
        get;
    }

    public string SubItemProp2
    {
        get;
    }

    public SubItemViewModel(
        string subItemProp1,
        string subItemProp2)
    {
        SubItemProp1 = subItemProp1;
        SubItemProp2 = subItemProp2;
    }
}

public class ItemViewModel : INotifyPropertyChanged
{
    string m_itemProp1;
    SubItemViewModel[] m_subItems;

    public event PropertyChangedEventHandler? PropertyChanged;

    public string ItemProp1
    {
        get => m_itemProp1;
        set
        {
            m_itemProp1 = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(ItemProp1)));
        }
    }

    public SubItemViewModel[] SubItems
    {
        get => m_subItems;
        set
        {
            m_subItems = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(SubItems)));
        }
    }

    public ItemViewModel()
    {
        m_itemProp1 = "";
        m_subItems = Array.Empty<SubItemViewModel>();
    }
}

public partial class MainWindow : Window, INotifyPropertyChanged
{
    const double DEFAULT_FONT_SIZE = 12;

    double m_itemsFontSize;
    ItemViewModel[] m_items;

    public event PropertyChangedEventHandler? PropertyChanged;

    public double ItemsFontSize
    {
        get => m_itemsFontSize;
        set
        {
            m_itemsFontSize = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(ItemsFontSize)));
        }
    }

    public ItemViewModel[] Items
    {
        get => m_items;
        set
        {
            m_items = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(Items)));
        }
    }

    public ICommand IncreaseFontSizeCommand
    {
        get => new RelayCommand(IncreaseFontSize);
    }

    public ICommand DecreaseFontSizeCommand
    {
        get => new RelayCommand(DecreaseFontSize);
    }

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;

        m_itemsFontSize = DEFAULT_FONT_SIZE;
        m_items = Array.Empty<ItemViewModel>();

        Loaded += HandleLoad;
    }

    void HandleLoad(object sender, RoutedEventArgs e)
    {
        Items = new[]
        {
            new ItemViewModel()
            {
                ItemProp1 = "ITEM1",
                SubItems = new[]
                {
                    new SubItemViewModel(
                        "SUBITEM1",
                        "SUBITEM2")
                }
            },
            new ItemViewModel()
            {
                ItemProp1 = "ITEM2",
                SubItems = new[]
                {
                    new SubItemViewModel(
                        "SUBITEM3",
                        "SUBITEM4")
                }
            },
            new ItemViewModel()
            {
                ItemProp1 = "ITEM3",
                SubItems = new[]
                {
                    new SubItemViewModel(
                        "SUBITEM5",
                        "SUBITEM6"),
                    new SubItemViewModel(
                        "SUBITEM7",
                        "SUBITEM8"),
                    new SubItemViewModel(
                        "SUBITEM9",
                        "SUBITEM10"),
                }
            }
        };
    }

    void IncreaseFontSize()
    {
        ++ItemsFontSize;
    }

    void DecreaseFontSize()
    {
        if (ItemsFontSize <= 3)
        {
            return;
        }

        --ItemsFontSize;
    }
}
